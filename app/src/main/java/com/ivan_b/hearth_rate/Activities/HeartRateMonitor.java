package com.ivan_b.hearth_rate.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ivan_b.hearth_rate.ImageProcessing;
import com.ivan_b.hearth_rate.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class HeartRateMonitor extends ActionBarActivity {

    private static final String TAG = "HeartRateMonitor"; // For logging
    private static final AtomicBoolean processing = new AtomicBoolean(false);

    private static SurfaceView preview = null;          //Camera surface view
    private static SurfaceHolder previewHolder = null;  //camera holder
    private static Camera camera = null;
    private static View image = null;
    private static TextView text = null;                //Rates displayed here

    //private static ArrayList<Integer> ints = new ArrayList<Integer>(1500);

    private static WakeLock wakeLock = null;            //Disable screen fading while test

    private static int averageIndex = 0;
    private static final int averageArraySize = 4;
    private static final int[] averageArray = new int[averageArraySize];


    public static enum TYPE {                           //Circle indicator types
        GREEN, RED
    };

    private static TYPE currentType = TYPE.GREEN;       //Current circle indicator

    public static TYPE getCurrent() {
        return currentType;
    }

    private static int beatsIndex = 0;
    private static final int beatsArraySize = 3;
    private static final int[] beatsArray = new int[beatsArraySize];
    private static double beats = 0;
    private static long startTime = 0;


    public static GraphicalView graphicalView;
    private Button stopRecording;
    static int nofdpoints = 0;
    public static Context c;

    static int count;

    /* Configure variables for chart */
    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    static XYSeries series = new XYSeries("heart rate");
    public static final int CHART_AXIS_TITLE_SIZE = 50;
    public static final int CHART_TITLE_SIZE = 100;
    public static final int CHART_LABELS_TEXT_SIZE = 30;
    public static final int CHART_LEGEND_TEXT_SIZE = 50;
    public static final float CHART_POINT_SIZE = 10f;

    //Sockets
    Socket socket;
    OutputStream out;
    PrintWriter output;
    boolean connected = false;

    String host;
    int port = 0;

    private XYMultipleSeriesRenderer getDemoRenderer() {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(HeartRateMonitor.CHART_AXIS_TITLE_SIZE);
        renderer.setChartTitleTextSize(HeartRateMonitor.CHART_TITLE_SIZE);
        renderer.setLabelsTextSize(HeartRateMonitor.CHART_LABELS_TEXT_SIZE);
        renderer.setLegendTextSize(HeartRateMonitor.CHART_LEGEND_TEXT_SIZE);
        renderer.setPointSize(HeartRateMonitor.CHART_POINT_SIZE);
        renderer.setMarginsColor(Color.WHITE);
        renderer.setMargins(new int[]{20, 30, 15, 0});
        XYSeriesRenderer r = new XYSeriesRenderer();
        r.setColor(Color.BLUE);
        r.setPointStyle(PointStyle.SQUARE);
        r.setFillBelowLine(true);
        r.setFillBelowLineColor(Color.WHITE);
        r.setFillPoints(true);
        renderer.addSeriesRenderer(r);
        return renderer;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        c = this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dataset.addSeries(series);
        graphicalView = ChartFactory.getScatterChartView(this, dataset, getDemoRenderer());
        LinearLayout layout = (LinearLayout) findViewById(R.id.graphlayout);
        layout.addView(graphicalView);

        stopRecording = (Button) findViewById(R.id.stop);

        preview = (SurfaceView) findViewById(R.id.preview);
        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        image = findViewById(R.id.image);
        text = (TextView) findViewById(R.id.text);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
        stopRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchDoneActivity(getApplicationContext());
                finish();
            }
        });

        try
        {
            host = getIntent().getStringExtra("host");
            port = getIntent().getIntExtra("port", 0);
        }
        catch(NumberFormatException e)
        {
            //ignored
        }


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(host != null && port != 0)
        {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        socket = new Socket(host, port);
                        out = socket.getOutputStream();
                        output = new PrintWriter(out);

                        connected = true;
                        Toast.makeText(getApplicationContext(),"CONNECTED with "+host, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.i(TAG, "SendDataToNetwork: Message send failed. Caught an exception");
                    }
                }
            }
            ).start();
        }
        else
            connected = false;


        wakeLock.acquire();
        camera = Camera.open();
        startTime = System.currentTimeMillis();
    }

    @Override
    public void onPause() {
        super.onPause();
        wakeLock.release();
        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;

        if(connected && socket != null)
            try {
                output.write("close");
                output.flush();

                out.close();
                output.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        connected = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected static void writeFile() {
        File file = new File(Environment.getExternalStorageDirectory() + "/results.txt");
        FileOutputStream stream = null;
        try {
            if (!file.exists()) {
                file.createNewFile();

            }

            stream = new FileOutputStream(file, true);
            PrintStream ps = new PrintStream(stream);
            ps.print("Subject\n");


            for (int i = 0; i < series.getItemCount(); i++) {
                ps.print("Average Heart Rate " + series.getY(i) + "\n");
            }
            stream.flush();
            stream.close();
            Log.d("Success", "Printed values to text file");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static double[] getRates() {
        double[] values = new double[nofdpoints];
        for (int i = 0; i < series.getItemCount(); i++) {
            values[i] = series.getY(i);
        }
        return values;
    }

    public static void launchDoneActivity(Context c) {
        Intent i = new Intent();

        i.setClass(c, DoneActivity.class);
        Bundle bundle = new Bundle();
        bundle.putDoubleArray("ratesbundle", getRates());
        i.putExtra("rates", bundle);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        writeFile();
        c.startActivity(i);
    }

    private PreviewCallback previewCallback = new PreviewCallback() {

        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {

            if (data == null)
                throw new NullPointerException();
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null)
                throw new NullPointerException();

            if (!processing.compareAndSet(false, true))
                return;

            int width = size.width;
            int height = size.height;

            int imgAvg = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), height, width);

            //ints.add(imgAvg);
            // Log.i(TAG, "imgAvg="+imgAvg);
            if (imgAvg == 0 || imgAvg == 255) {
                processing.set(false);
                return;
            }



            int averageArrayAvg = 0;
            int averageArrayCnt = 0;
            for (int i = 0; i < averageArray.length; i++) {
                if (averageArray[i] > 0) {
                    averageArrayAvg += averageArray[i];
                    averageArrayCnt++;
                }
            }

            int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt) : 0;
            TYPE newType = currentType;
            if (imgAvg < rollingAverage) {
                newType = TYPE.RED;
                if (newType != currentType) {
                    beats++;
                    // Log.d(TAG, "BEAT!! beats="+beats);
                    output.println("Beat...");
                    output.flush();
                }
            } else if (imgAvg > rollingAverage) {
                newType = TYPE.GREEN;
            }

            if (averageIndex == averageArraySize) averageIndex = 0;
            averageArray[averageIndex] = imgAvg;
            averageIndex++;

            // Switch circle to new state
            if (newType != currentType) {
                currentType = newType;
                image.postInvalidate();
            }


            long endTime = System.currentTimeMillis();
            double totalTimeInSecs = (endTime - startTime) / 1000d;
            if (totalTimeInSecs >= 10) {
                double bps = (beats / totalTimeInSecs);
                int dpm = (int) (bps * 60d);
                if (dpm < 30 || dpm > 180) {
                    startTime = System.currentTimeMillis();
                    beats = 0;
                    processing.set(false);
                    return;

                }

                // Log.d(TAG,
                // "totalTimeInSecs="+totalTimeInSecs+" beats="+beats);

                if (beatsIndex == beatsArraySize)
                    beatsIndex = 0;
                beatsArray[beatsIndex] = dpm;
                beatsIndex++;

                int beatsArrayAvg = 0;
                int beatsArrayCnt = 0;
                for (int i = 0; i < beatsArray.length; i++) {
                    if (beatsArray[i] > 0) {
                        beatsArrayAvg += beatsArray[i];
                        beatsArrayCnt++;
                    }
                }

                int beatsAvg = (beatsArrayAvg / beatsArrayCnt);
                text.setText(String.valueOf(beatsAvg));

                output.println(beatsAvg);
                output.flush();

                series.add(nofdpoints, beatsAvg);
                nofdpoints++;
                count++;
                graphicalView.repaint();
                startTime = System.currentTimeMillis();
                beats = 0;
                toneGenerator toneGenerator=new toneGenerator();
                toneGenerator.playTone();
            }
            processing.set(false);
        }

    };

    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(previewHolder);
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback", "Exception in setPreviewDisplay()", t);
            }
        }


        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            Camera.Size size = getSmallestPreviewSize(width, height, parameters);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                Log.d(TAG, "Using width=" + size.width + " height=" + size.height);
            }
            camera.setParameters(parameters);
            camera.startPreview();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Ignore
        }
    };

    private static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea < resultArea) result = size;
                }
            }
        }

        return result;
    }
}


class toneGenerator {
    private final int duration = 1; // seconds
    private final int sampleRate = 8000;
    private final int numSamples = duration * sampleRate;
    private final double sample[] = new double[numSamples];
    private final double freqOfTone = 440; // hz

    private final byte generatedSnd[] = new byte[2 * numSamples];
    //handler used to
    Handler handler = new Handler();


    public  void playTone(){
        final Thread thread = new Thread(new Runnable() {
            public void run() {
                genTone();
                handler.post(new Runnable() {

                    public void run() {
                        playSound();
                    }
                });
            }
        });
        thread.start();
    }
    //method to generate the tone using simple math with frequency calculation
    void genTone() {
        // fill out the array
        for (int i = 0; i < numSamples; ++i) {
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate / freqOfTone));
        }

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        for (final double dVal : sample) {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);

        }
    }
    //Method which will actually play the tone when called.
    void playSound() {
        final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                AudioFormat.ENCODING_PCM_16BIT, numSamples,
                AudioTrack.MODE_STATIC);
        audioTrack.write(generatedSnd, 0, generatedSnd.length);
        audioTrack.play();
    }
}