package com.ivan_b.hearth_rate.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ivan_b.hearth_rate.R;


public class Home extends Activity implements View.OnClickListener {

    Button quickMeasurement;
    EditText hostEdit;
    EditText portEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        hostEdit = (EditText)findViewById(R.id.hostEdit);
        portEdit = (EditText)findViewById(R.id.portEdit);

        quickMeasurement = (Button) findViewById(R.id.quickMeasurementButton);
        quickMeasurement.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.quickMeasurementButton:
                intent = new Intent(this, HeartRateMonitor.class);
                intent.putExtra("host", hostEdit.getText().toString());
                intent.putExtra("port", Integer.valueOf(portEdit.getText().toString()));
                startActivity(intent);
                break;


        }
    }


}
