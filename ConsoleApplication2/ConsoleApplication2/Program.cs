﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace SocketServer
{
	class Program
	{
		static void Main(string[] args)
		{
			IPHostEntry ipHost;
			IPAddress ipAddr = null;
			IPEndPoint ipEndPoint = null;
			Socket sListener = null;

			Console.WriteLine("Enter \"1\" for standart connection or \"2\" for custom connection");

			switch (Console.ReadKey().KeyChar)
			{
				case '1':
					{
						ipHost = Dns.GetHostEntry("192.168.0.100");
						ipAddr = ipHost.AddressList[1];
						ipEndPoint = new IPEndPoint(ipAddr, 3000);
						sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
						break;
					}

				case '2':
					{
						Console.WriteLine("\nEnter ip and port (i.e 192.168.0.100:3000)");
						string str = Console.ReadLine();
						string host = str.Substring(0, str.IndexOf(':'));
						int port = Convert.ToInt32(str.Substring(str.IndexOf(':')+1, str.Length - str.IndexOf(':')-1 ));

						ipAddr = IPAddress.Parse(host);
						ipEndPoint = new IPEndPoint(ipAddr, port);
						sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

						break;
					}
			}


			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("\nServer started at " + ipEndPoint);
			Console.ResetColor();
			Console.WriteLine("_____________________________________");

			try
			{
				sListener.Bind(ipEndPoint);
				sListener.Listen(10);


				Socket vassa = null;
				restart(vassa, sListener);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			finally
			{
				Console.ReadLine();
			}
		}

		public static void restart(Socket socket, Socket handler)
		{
			socket = handler.Accept();
			string data = null;
			byte[] bytes = new byte[1024];
			int bytesRec = 0;

			int beats = 0;

			while (true)
			{
				bytesRec = socket.Receive(bytes);
				data = Encoding.UTF8.GetString(bytes, 0, bytesRec);

				if (data.IndexOf("close") > -1)
				{
					Console.WriteLine("Connection ended with client");
					socket.Shutdown(SocketShutdown.Both);
					socket.Close();

					try
					{
						socket.Dispose();
						socket = null;
					}
					catch (Exception) { };

					restart(socket, handler);
					break;
				}
				else
				{
					if (data.IndexOf("B") == -1)
					{
						Console.ForegroundColor = ConsoleColor.Red;
						Console.WriteLine(String.Format("\n__________[ {0} BPM ]__________", data.Trim()));
						Console.ResetColor();

					}
					else
						Console.Write(".");

				}


			}
		}

	}
}